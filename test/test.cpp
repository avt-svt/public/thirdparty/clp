/**********************************************************************************
 * Copyright (c) 2019 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This file is made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * @file test.cpp
 *
 * @brief Test file for CLP.
 *
 *  Maximize
 *
 *	Z = f(x,y) = 3x + 2y
 *
 * subject to:
 *
 *	2x + y ≤ 18
 *	2x + 3y ≤ 42
 *	3x + y ≤ 24
 *
 *	x ≥ 0 , y ≥ 0
 *
 **********************************************************************************/

#include "ClpSimplex.hpp"
#include "ClpFactorization.hpp"
#include "ClpNetworkMatrix.hpp"

#include <stdio.h>
#include <float.h>
#include <limits>


using namespace std;


int main(int argc, const char *argv[])
{

	constexpr int numrows = 3, numcolumns = 2;
	int i, j;

	double tlower[numrows] = {- std::numeric_limits<double>::max(), -std::numeric_limits<double>::max(), -std::numeric_limits<double>::max() };
	double tupper[numrows] = {18, 42, 24};

	double tlowerColumn[numcolumns] = {0, 0};
	double tupperColumn[numcolumns] = { std::numeric_limits<double>::max(), std::numeric_limits<double>::max() };

	double tobjective[numcolumns] = {3, 2};



     double * lower = new double[numrows];
     double * upper = new double[numrows];

	for (i=0;i<numrows;i++){
		upper[i] = tupper[i];
		lower[i] = tlower[i];
	}


     double * objective = new double[numcolumns];
     double * lowerColumn = new double[numcolumns];
     double * upperColumn = new double[numcolumns];

	for (i=0;i<numcolumns;i++){
		objective[i] = tobjective[i];
		upperColumn[i] = tupperColumn[i];
		lowerColumn[i] = tlowerColumn[i];
	}

     double * value = new double [numrows*numcolumns];
     int * start = new int[numcolumns+1];
     int * index = new int[numrows*numcolumns];

	value[0] = 2;
	value[1] = 2;
	value[2] = 3;
	value[3] = 1;
	value[4] = 3;
	value[5] = 1;

	int count =0;
	for(i=0;i<numcolumns;i++)
		for(j=0;j<numrows;j++){
			index[count] = j;
			count++;
		}

	for(i=0;i<=numcolumns;i++){
		start[i] = i*numrows;
	}

    CoinPackedMatrix matrix;
     int * lengths = NULL;
     matrix.assignMatrix(true, numrows, numcolumns,
                         numcolumns*numrows, value, index, start, lengths);

	const double * a = matrix.getElements();
	const int * b = matrix.getIndices();
	const int * c = matrix.getVectorStarts();
	double d = matrix.getNumElements();
	double e = matrix.getMajorDim();

	for(i=0; i<numcolumns*numrows; i++)
		cout << a[i] << ' ';

	cout <<endl;
	for(i=0; i<numcolumns*numrows; i++)
		cout << b[i] << ' ';

	cout <<endl;
	for(i=0; i<numcolumns+1; i++)
		cout << c[i] << ' ';

	cout<<endl<<d<<"	"<<endl<<e;

	cout <<endl;

     ClpSimplex  model;

     model.loadProblem(matrix,
                       lowerColumn, upperColumn, objective,
                       lower, upper);

		cout <<endl;

     std::cout << "Model has "<< model.numberRows() << " rows and " <<
               model.numberColumns() << " columns" << std::endl;

	cout <<endl;

	model.messageHandler()->setLogLevel(1);

	model.setOptimizationDirection(-1);
	model.primal();
    model.createStatus();
	model.dual();
	cout<<endl<<"Obj: "<<model.objectiveValue()<<endl;

  int numberRows = model.numberRows();
  double * rowPrimal = model.primalRowSolution();
  double * rowDual = model.dualRowSolution();

   int iRow;

   for (iRow=0;iRow<numberRows;iRow++)
    cout<<endl<<"	Row	"<<iRow << "	primal	"<<rowPrimal[iRow]<<"	dual	"<<rowDual[iRow]<<endl;


  int numberColumns = model.numberColumns();
  double * columnPrimal = model.primalColumnSolution();
  double * columnDual = model.dualColumnSolution();


  int iColumn;

  for (iColumn=0;iColumn<numberColumns;iColumn++)
    cout<<endl<<"	Column	"<<iColumn << "	primal	"<<columnPrimal[iColumn]<<"	dual	"<<columnDual[iColumn]<<endl;
     return 0;

}