The folder Clp-1.17.0 contains Clp version 1.17.0 as downloaded on April 16, 2019 from https://www.coin-or.org/download/source/Clp/
For license information, please refer to the files therein.

This Clp version has been tested for the following configurations:   
	- Windows 7 using Microsoft Visual Studio 2017  
	- Red Hat Linux 8.2.1  using gcc 8.2.1  