set(CLP_SOURCES
#   ${CLP_SRCDIR}/AbcDualRowDantzig.cpp
#	${CLP_SRCDIR}/AbcWarmStart.cpp
	${CLP_SRCDIR}/ClpConstraintLinear.cpp
	${CLP_SRCDIR}/ClpInterior.cpp
	${CLP_SRCDIR}/ClpPdco.cpp
	${CLP_SRCDIR}/ClpQuadraticObjective.cpp
#	${CLP_SRCDIR}/CoinAbcDenseFactorization.cpp
#	${CLP_SRCDIR}/CoinAbcSmallFactorization2.cpp
#	${CLP_SRCDIR}/AbcDualRowPivot.cpp
	${CLP_SRCDIR}/CbcOrClpParam.cpp
	${CLP_SRCDIR}/ClpConstraintQuadratic.cpp
	${CLP_SRCDIR}/ClpLinearObjective.cpp
	${CLP_SRCDIR}/ClpPdcoBase.cpp
	${CLP_SRCDIR}/ClpSimplex.cpp
#	${CLP_SRCDIR}/CoinAbcFactorization1.cpp
#	${CLP_SRCDIR}/CoinAbcSmallFactorization3.cpp
#	${CLP_SRCDIR}/AbcDualRowSteepest.cpp
	${CLP_SRCDIR}/Clp_ampl.cpp
	${CLP_SRCDIR}/ClpDualRowDantzig.cpp
	${CLP_SRCDIR}/ClpLsqr.cpp
	${CLP_SRCDIR}/ClpPEDualRowDantzig.cpp
	${CLP_SRCDIR}/ClpSimplexDual.cpp
#	${CLP_SRCDIR}/CoinAbcFactorization2.cpp
#	${CLP_SRCDIR}/CoinAbcSmallFactorization4.cpp
#	${CLP_SRCDIR}/AbcMatrix.cpp
	${CLP_SRCDIR}/Clp_C_Interface.cpp
	${CLP_SRCDIR}/ClpDualRowPivot.cpp
	${CLP_SRCDIR}/ClpMain.cpp
	${CLP_SRCDIR}/ClpPEDualRowSteepest.cpp
	${CLP_SRCDIR}/ClpSimplexNonlinear.cpp
#	${CLP_SRCDIR}/CoinAbcFactorization3.cpp
#	${CLP_SRCDIR}/CoinAbcSmallFactorization5.cpp
#	${CLP_SRCDIR}/AbcNonLinearCost.cpp
	${CLP_SRCDIR}/ClpCholeskyBase.cpp
	${CLP_SRCDIR}/ClpDualRowSteepest.cpp
	${CLP_SRCDIR}/ClpMatrixBase.cpp
	${CLP_SRCDIR}/ClpPEPrimalColumnDantzig.cpp
	${CLP_SRCDIR}/ClpSimplexOther.cpp
#	${CLP_SRCDIR}/CoinAbcFactorization4.cpp
	${CLP_SRCDIR}/Idiot.cpp
#	${CLP_SRCDIR}/AbcPrimalColumnDantzig.cpp
	${CLP_SRCDIR}/ClpCholeskyDense.cpp
	${CLP_SRCDIR}/ClpDummyMatrix.cpp
	${CLP_SRCDIR}/ClpMessage.cpp
	${CLP_SRCDIR}/ClpPEPrimalColumnSteepest.cpp
	${CLP_SRCDIR}/ClpSimplexPrimal.cpp
#	${CLP_SRCDIR}/CoinAbcFactorization5.cpp
	${CLP_SRCDIR}/IdiSolve.cpp
#	${CLP_SRCDIR}/AbcPrimalColumnPivot.cpp
	${CLP_SRCDIR}/ClpCholeskyMumps.cpp
	${CLP_SRCDIR}/ClpDynamicExampleMatrix.cpp
	${CLP_SRCDIR}/ClpModel.cpp
	${CLP_SRCDIR}/ClpPESimplex.cpp
	${CLP_SRCDIR}/ClpSolve.cpp
#	${CLP_SRCDIR}/CoinAbcHelperFunctions.cpp
	${CLP_SRCDIR}/MyEventHandler.cpp
#	${CLP_SRCDIR}/AbcPrimalColumnSteepest.cpp
	${CLP_SRCDIR}/ClpCholeskyPardiso.cpp
	${CLP_SRCDIR}/ClpDynamicMatrix.cpp
	${CLP_SRCDIR}/ClpNetworkBasis.cpp
	${CLP_SRCDIR}/ClpPlusMinusOneMatrix.cpp
	${CLP_SRCDIR}/ClpSolver.cpp
#	${CLP_SRCDIR}/CoinAbcOrderedFactorization1.cpp
	${CLP_SRCDIR}/MyMessageHandler.cpp
#	${CLP_SRCDIR}/AbcSimplex.cpp
	${CLP_SRCDIR}/ClpCholeskyTaucs.cpp
	${CLP_SRCDIR}/ClpEventHandler.cpp
	${CLP_SRCDIR}/ClpNetworkMatrix.cpp
	${CLP_SRCDIR}/ClpPredictorCorrector.cpp
#	${CLP_SRCDIR}/CoinAbcBaseFactorization1.cpp
#	${CLP_SRCDIR}/CoinAbcOrderedFactorization2.cpp
	${CLP_SRCDIR}/unitTest.cpp
#	${CLP_SRCDIR}/AbcSimplexDual.cpp
#	${CLP_SRCDIR}/ClpCholeskyUfl.cpp
	${CLP_SRCDIR}/ClpFactorization.cpp
	${CLP_SRCDIR}/ClpNode.cpp
	${CLP_SRCDIR}/ClpPresolve.cpp
#	${CLP_SRCDIR}/CoinAbcBaseFactorization2.cpp
#	${CLP_SRCDIR}/CoinAbcOrderedFactorization3.cpp
#	${CLP_SRCDIR}/AbcSimplexFactorization.cpp
#	${CLP_SRCDIR}/ClpCholeskyWssmp.cpp
	${CLP_SRCDIR}/ClpGubDynamicMatrix.cpp
	${CLP_SRCDIR}/ClpNonLinearCost.cpp
	${CLP_SRCDIR}/ClpPrimalColumnDantzig.cpp
#	${CLP_SRCDIR}/CoinAbcBaseFactorization3.cpp
#	${CLP_SRCDIR}/CoinAbcOrderedFactorization4.cpp
#	${CLP_SRCDIR}/AbcSimplexParallel.cpp
#	${CLP_SRCDIR}/ClpCholeskyWssmpKKT.cpp
	${CLP_SRCDIR}/ClpGubMatrix.cpp
	${CLP_SRCDIR}/ClpObjective.cpp
	${CLP_SRCDIR}/ClpPrimalColumnPivot.cpp
#	${CLP_SRCDIR}/CoinAbcBaseFactorization4.cpp
#	${CLP_SRCDIR}/CoinAbcOrderedFactorization5.cpp
#	${CLP_SRCDIR}/AbcSimplexPrimal.cpp
	${CLP_SRCDIR}/ClpConstraint.cpp
	${CLP_SRCDIR}/ClpHelperFunctions.cpp
	${CLP_SRCDIR}/ClpPackedMatrix.cpp
	${CLP_SRCDIR}/ClpPrimalColumnSteepest.cpp
#	${CLP_SRCDIR}/CoinAbcBaseFactorization5.cpp
#	${CLP_SRCDIR}/CoinAbcSmallFactorization1.cpp
)