set(COINUTILS_SOURCES
    ${CLP_COINDIR}/CoinAlloc.cpp
	${CLP_COINDIR}/CoinFactorization4.cpp
	${CLP_COINDIR}/CoinModelUseful.cpp
	${CLP_COINDIR}/CoinPackedVectorBase.cpp
	${CLP_COINDIR}/CoinPresolveEmpty.cpp
#	${CLP_COINDIR}/CoinPresolvePsdebug.cpp
	${CLP_COINDIR}/CoinSearchTree.cpp
	${CLP_COINDIR}/CoinWarmStartVector.cpp
	${CLP_COINDIR}/CoinBuild.cpp
	${CLP_COINDIR}/CoinFileIO.cpp
	${CLP_COINDIR}/CoinModelUseful2.cpp
	${CLP_COINDIR}/CoinParam.cpp
	${CLP_COINDIR}/CoinPresolveFixed.cpp
	${CLP_COINDIR}/CoinPresolveSingleton.cpp
	${CLP_COINDIR}/CoinShallowPackedVector.cpp
	${CLP_COINDIR}/CoinDenseFactorization.cpp
	${CLP_COINDIR}/CoinFinite.cpp
	${CLP_COINDIR}/CoinMpsIO.cpp
	${CLP_COINDIR}/CoinParamUtils.cpp
	${CLP_COINDIR}/CoinPresolveForcing.cpp
	${CLP_COINDIR}/CoinPresolveSubst.cpp
	${CLP_COINDIR}/CoinSimpFactorization.cpp
	${CLP_COINDIR}/CoinDenseVector.cpp
	${CLP_COINDIR}/CoinIndexedVector.cpp
	${CLP_COINDIR}/CoinOslFactorization.cpp
	${CLP_COINDIR}/CoinPostsolveMatrix.cpp
	${CLP_COINDIR}/CoinPresolveHelperFunctions.cpp
	${CLP_COINDIR}/CoinPresolveTighten.cpp
	${CLP_COINDIR}/CoinSnapshot.cpp
	${CLP_COINDIR}/CoinError.cpp
	${CLP_COINDIR}/CoinLpIO.cpp
	${CLP_COINDIR}/CoinOslFactorization2.cpp
	${CLP_COINDIR}/CoinPrePostsolveMatrix.cpp
	${CLP_COINDIR}/CoinPresolveImpliedFree.cpp
	${CLP_COINDIR}/CoinPresolveTripleton.cpp
	${CLP_COINDIR}/CoinStructuredModel.cpp
	${CLP_COINDIR}/CoinFactorization1.cpp
	${CLP_COINDIR}/CoinMessage.cpp
	${CLP_COINDIR}/CoinOslFactorization3.cpp
	${CLP_COINDIR}/CoinPresolveDoubleton.cpp
	${CLP_COINDIR}/CoinPresolveIsolated.cpp
	${CLP_COINDIR}/CoinPresolveUseless.cpp
	${CLP_COINDIR}/CoinWarmStartBasis.cpp
	${CLP_COINDIR}/CoinFactorization2.cpp
	${CLP_COINDIR}/CoinMessageHandler.cpp
	${CLP_COINDIR}/CoinPackedMatrix.cpp
	${CLP_COINDIR}/CoinPresolveDual.cpp
	${CLP_COINDIR}/CoinPresolveMatrix.cpp
	${CLP_COINDIR}/CoinPresolveZeros.cpp
	${CLP_COINDIR}/CoinWarmStartDual.cpp
	${CLP_COINDIR}/CoinFactorization3.cpp
	${CLP_COINDIR}/CoinModel.cpp
	${CLP_COINDIR}/CoinPackedVector.cpp
	${CLP_COINDIR}/CoinPresolveDupcol.cpp
	${CLP_COINDIR}/CoinPresolveMonitor.cpp
	${CLP_COINDIR}/CoinRational.cpp
	${CLP_COINDIR}/CoinWarmStartPrimalDual.cpp
)